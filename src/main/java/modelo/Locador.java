package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import java.util.List;
import excepcion.BadPostCodeException;

@Entity
@PrimaryKeyJoinColumn(name="fk_cliente")
@Table(name="Locador")
public class Locador extends Cliente implements java.io.Serializable {
	
	  	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="cuit")
		private String cuit;
	@Column(name="razonSocial")
	    private String razonSocial;
	 @OneToMany(mappedBy="locador")
	private List<Inmueble> inmueble;
	
	public Locador(){}
	public Locador(String tipoDeDocumento, String documento, String nombre, String apellido, String estadoCivil,
			String domicilio, String telefono, String email, String cuit, String razonSocial) throws BadPostCodeException {
		super(tipoDeDocumento, documento, nombre, apellido, estadoCivil, domicilio, telefono, email);
		this.cuit = cuit;
		this.razonSocial = razonSocial;
		
		if(!validarCuit()) {
			throw new BadPostCodeException("\"El campo de CUIT no puede estar vacio.");
		}else if(!validarRazonSocial()) {
			throw new BadPostCodeException("El campo de razon social no puede estar vacio.");
		}
		
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String toString(){
		return "Datos de Locador: " + this.getApellido() + " " + this.getNombre() +", "+ this.getTipoDeDocumento() + ", "+ this.getDocumento()+ ", "+this.getEstadoCivil()+", " +this.getDomicilio() +
				", " + this.getEmail() +", " + this.getTelefono() +", "+ cuit +", " + razonSocial;
	//String cuit, String razonSocial
	}
	
	
	
	
	/*------------------Metodos de validar Locador--------------------------------------*/
	public boolean validarCuit(){
 		boolean valido = false;
 		if(this.cuit.length()>6 && this.cuit.length()<12){
 			valido = true;
 		}
 		return valido;
 	}
	public boolean validarRazonSocial(){
 		boolean valido = false;
 		if(this.razonSocial.length()>6 && this.razonSocial.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	    
	    
}
