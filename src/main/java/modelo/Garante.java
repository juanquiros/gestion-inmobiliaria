package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import excepcion.BadPostCodeException;

@Entity
@PrimaryKeyJoinColumn(name="fk_cliente")
@Table(name="Garantes")
public class Garante extends Cliente implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="activadaQueRealiza")
	private String activadaQueRealiza;
	@Column(name="comprobanteIngresos")
    private String comprobanteIngresos;    
	
	@OneToOne(mappedBy="garante")
	private Alquiler alquiler;
	
	public Garante() {}	
	public Garante(String tipoDeDocumento, String documento, String nombre, String apellido, String estadoCivil,
			String domicilio, String telefono, String email, String activadaQueRealiza, String comprobanteIngresos) throws BadPostCodeException {
		super(tipoDeDocumento, documento, nombre, apellido, estadoCivil, domicilio, telefono, email);
		this.activadaQueRealiza = activadaQueRealiza;
		this.comprobanteIngresos = comprobanteIngresos;
		
		if(!validarActividadGarante()) {
			throw new BadPostCodeException("El campo de  actividad que realiza el Garante   no puede estar vacio.");
		}else if(!validarComprobanteIngreso()){
			throw new BadPostCodeException("\"El campo de comprobante de ingresos no puede estar vacio.");
		}
	}

	public String getActivadaQueRealiza() {
		return activadaQueRealiza;
	}
	public void setActivadaQueRealiza(String activadaQueRealiza) {
		this.activadaQueRealiza = activadaQueRealiza;
	}
	public String getComprobanteIngresos() {
		return comprobanteIngresos;
	}
	public void setComprobanteIngresos(String comprobanteIngresos) {
		this.comprobanteIngresos = comprobanteIngresos;
	}
    public String toString(){
    	return "Datos garante: " + getApellido() + " " + getNombre() + ", " + getTipoDeDocumento()+":"+getDocumento()+", "+ getEstadoCivil() +","+getTelefono()+", "+getDomicilio()+", " + getEmail() + " " + getComprobanteIngresos() + ", " + getActivadaQueRealiza();
    	
    }
    
    
    /*-----------------------Metodos de validar Garante-------------------------------------*/
    public boolean validarActividadGarante(){
 		boolean valido = false;
 		if(this.activadaQueRealiza.length()>6 && this.activadaQueRealiza.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
    
    public boolean validarComprobanteIngreso(){
 		boolean valido = false;
 		if(this.comprobanteIngresos.length()>6 && this.comprobanteIngresos.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
}
