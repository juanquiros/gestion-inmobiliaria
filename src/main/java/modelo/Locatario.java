package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import excepcion.BadPostCodeException;
@Entity
@PrimaryKeyJoinColumn(name="fk_cliente")
@Table(name="Locatario")
public class Locatario extends Cliente implements java.io.Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="actividadQueRealiza")
	 	private String actividadQueRealiza;
	@Column(name="comprobanteIngreso")
	    private String comprobanteIngreso; //descripcion de que papel entrego
	@Column(name="usuarioWeb")
	    private String usuarioWeb;
	@Column(name="contrasenia")
	    private String contrasenia;
	
	@OneToOne(mappedBy="locatario")
	private Alquiler alquiler;
	
	@OneToOne(mappedBy="unLocatario")
	private CuentaCorrienteCliente cuentaCorrienteCliente;
	
	public Locatario(){}
	public Locatario(String tipoDeDocumento, String documento, String nombre, String apellido, String estadoCivil,
			String domicilio, String telefono, String email,String actividadQueRealiza, String comprobanteIngreso, String usuarioWeb, String contrasenia) throws BadPostCodeException {
		super(tipoDeDocumento, documento, nombre, apellido, estadoCivil, domicilio, telefono, email);
		this.actividadQueRealiza = actividadQueRealiza;
		this.comprobanteIngreso = comprobanteIngreso;
		this.usuarioWeb = usuarioWeb;
		this.contrasenia = contrasenia;
		
		if(!validarActividadLocatario()) {
			throw new BadPostCodeException("El campo de  actividad que realiza el Locatario   no puede estar vacio.");
		}else if(!validarComprobanteIngreso()) {
			throw new BadPostCodeException("\"El campo de comprobante de ingresos no puede estar vacio.");
		}else if(!validarUsuarioWeb()) {
			throw new BadPostCodeException("El campo de usuario web no puede estar vacio.");
		}else if(!validarContraseniaWeb()) {
			throw new BadPostCodeException("El campo de contraseniano puede estar vacio.");
		}
		
		
	}
	public String getActividadQueRealiza() {
		return actividadQueRealiza;
	}
	public void setActividadQueRealiza(String actividadQueRealiza) {
		this.actividadQueRealiza = actividadQueRealiza;
	}
	public String getComprobanteIngreso() {
		return comprobanteIngreso;
	}
	public void setComprobanteIngreso(String comprobanteIngreso) {
		this.comprobanteIngreso = comprobanteIngreso;
	}
	public String getUsuarioWeb() {
		return usuarioWeb;
	}
	public void setUsuarioWeb(String usuarioWeb) {
		this.usuarioWeb = usuarioWeb;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	public String toString(){
		return "Datos de locatario: " + this.getApellido() + " " + this.getNombre() +", "+ this.getTipoDeDocumento() + ", "+ this.getDocumento()+ ", "+this.getEstadoCivil()+", " +this.getDomicilio() +
				", " + this.getEmail() +", " + this.getTelefono() +", "+ this.getUsuarioWeb() + ", " + this.actividadQueRealiza + ", " + this.comprobanteIngreso +
				", " + this.usuarioWeb + ", " + this.contrasenia;
	//String actividadQueRealiza, String comprobanteIngreso, String usuarioWeb, String contrasenia
	}
	
	
	
	/*--------------------------Metodos de validar Locatario-----------------------------------------------*/
	public boolean validarActividadLocatario(){
 		boolean valido = false;
 		if(this.actividadQueRealiza.length()>6 && this.actividadQueRealiza.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarComprobanteIngreso(){
 		boolean valido = false;
 		if(this.comprobanteIngreso.length()>6 && this.comprobanteIngreso.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarUsuarioWeb(){
 		boolean valido = false;
 		if(this.usuarioWeb.length()>6 && this.usuarioWeb.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
	public boolean validarContraseniaWeb(){
 		boolean valido = false;
 		if(this.contrasenia.length()>6 && this.contrasenia.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
	
	
}
