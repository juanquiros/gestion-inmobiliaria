package modelo;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name ="Contrato")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Contrato implements java.io.Serializable{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="cod_contrato", unique=true, updatable=false,nullable =false)
	 	private int cod_contrato;
		
		@Column(name="fechaRealizado")
	 	private LocalDate fechaRealizado;
		
		@Column(name="duracion")
	    private int duracion;
		
		@Column(name="habilitado")
	    private boolean habilitado;
	    
		
		@OneToOne(cascade=CascadeType.ALL)
		@JoinColumn(name="ComicionInmobiliaria_cod_ComInmob")
	    private ComicionInmobiliaria comicionInmobiliari;
		
		@OneToOne(cascade=CascadeType.ALL)
		@JoinColumn(name="recargo_cod_recargo")
	    private Recargo recargo;    
	    
	    @OneToMany(cascade=CascadeType.ALL)
	    @JoinColumn(name="contrato_cod_contrato")
	    private Set<ArancelEspecial> arancelesEspeciales;
	    
	    
	    public Contrato(){}
		public Contrato(LocalDate fechaRealizado, int duracion, boolean habilitado,
				ComicionInmobiliaria comicionInmobiliari, Recargo recargo, Set<ArancelEspecial> arancelesEspeciales) {
			this.fechaRealizado = fechaRealizado;
			this.duracion = duracion;
			this.habilitado = habilitado;
			this.comicionInmobiliari = comicionInmobiliari;
			this.recargo = recargo;
			this.arancelesEspeciales = arancelesEspeciales;
		}
		public int getCod_contrato() {
			return cod_contrato;
		}
		public void setCod_contrato(int cod_contrato) {
			this.cod_contrato = cod_contrato;
		}
		public LocalDate getFechaRealizado() {
			return fechaRealizado;
		}
		public void setFechaRealizado(LocalDate fechaRealizado) {
			this.fechaRealizado = fechaRealizado;
		}
		public int getDuracion() {
			return duracion;
		}
		public void setDuracion(int duracion) {
			this.duracion = duracion;
		}
		public boolean isHabilitado() {
			return habilitado;
		}
		public void setHabilitado(boolean habilitado) {
			this.habilitado = habilitado;
		}
		public ComicionInmobiliaria getComicionInmobiliari() {
			return comicionInmobiliari;
		}
		public void setComicionInmobiliari(ComicionInmobiliaria comicionInmobiliari) {
			this.comicionInmobiliari = comicionInmobiliari;
		}
		public Recargo getUnRecargo() {
			return recargo;
		}
		public void setUnRecargo(Recargo unRecargo) {
			this.recargo = unRecargo;
		}
		public Set<ArancelEspecial> getArancelesEspeciales() {
			return arancelesEspeciales;
		}
		public void setArancelesEspeciales(Set<ArancelEspecial> arancelesEspeciales) {
			this.arancelesEspeciales = arancelesEspeciales;
		}
	    
	    public String decripcionContrato(){
	    	//LocalDate fechaRealizado, int duracion, boolean habilitado,
			//ComicionInmobiliaria comicionInmobiliari, Recargo unRecargo, Set<ArancelEspecial> arancelesEspeciales
	    	String arancelE = "";
	    	for (ArancelEspecial arancel: arancelesEspeciales){
	    		arancelE = arancelE + "\n" + arancel.toString();
	    	}
	    	return "Fecha registro: " + fechaRealizado.toString()+ "\nDuracion:" + duracion + "meses\nEstado:"+ habilitado + "\nComicion de Inmobiliaria:"+ comicionInmobiliari.toString() + "\n"+ recargo.toString() + "\n" + arancelE;
	    }
}
