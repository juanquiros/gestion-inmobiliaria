package modelo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="CuentaCorrienteCliente")
public class CuentaCorrienteCliente {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_ccc", unique=true, updatable=false,nullable =false)
	private int cod_ccc;
	
	@OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="cuenta_corriente_cliente_fk")
    private Set<Pago> Pagos;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="comprador_cod_comprador")
	private Comprador unComprador;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="locatario_cod_locatario")
	private Locatario unLocatario;
	
	

	public CuentaCorrienteCliente(Set<Pago> pagos, Comprador unComprador, Locatario unLocatario) {
		Pagos = pagos;
		this.unComprador = unComprador;
		this.unLocatario = unLocatario;
	}
	
	public CuentaCorrienteCliente() {}

	public Set<Pago> getPagos() {
		return Pagos;
	}

	public void setPagos(Set<Pago> pagos) {
		Pagos = pagos;
	}

	public Comprador getUnComprador() {
		return unComprador;
	}

	public void setUnComprador(Comprador unComprador) {
		this.unComprador = unComprador;
	}

	public Locatario getunLocatario() {
		return unLocatario;
	}

	public void setLocatario(Locatario unLocatario) {
		this.unLocatario = unLocatario;
	}
	
	
	
	
}
