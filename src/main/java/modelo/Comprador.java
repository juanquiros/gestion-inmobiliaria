package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import excepcion.BadPostCodeException;
@Entity
@PrimaryKeyJoinColumn(name="fk_cliente")
@Table(name="Comprador")
public class Comprador extends Cliente implements java.io.Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="activadaQueRealiza")
	 	private String activadaQueRealiza;
	@Column(name="comprobanteIngresos")
		private String comprobanteIngresos;
	@Column(name="usuarioWeb")
	    private String usuarioWeb;
	@Column(name="contraseniaWeb")
	    private String contraseniaWeb;
	
	@OneToOne(mappedBy="unComprador")
	private CuentaCorrienteCliente cuentaCorrienteCliente;
	
	public Comprador(){}

	public Comprador(String tipoDeDocumento, String documento, String nombre, String apellido, String estadoCivil,
			String domicilio, String telefono, String email,String activadaQueRealiza, String comprobanteIngresos, String usuarioWeb, String contraseniaWeb) throws BadPostCodeException {
		super(tipoDeDocumento, documento, nombre, apellido, estadoCivil, domicilio, telefono, email);
		this.activadaQueRealiza = activadaQueRealiza;
		this.comprobanteIngresos = comprobanteIngresos;
		this.usuarioWeb = usuarioWeb;
		this.contraseniaWeb = contraseniaWeb;
		
		if(!validarActividadQueRealiza()) {
			throw new BadPostCodeException("El campo de Actividad Que Realiza  no puede estar vacio.");
		}else if(!validarComprobanteIngreso()) {
			throw new BadPostCodeException("El campo de Comprobante de ingresos  no puede estar vacio.");
		}else if(!validarUsuarioWeb()) {
			throw new BadPostCodeException("El campo de Usuario Web  no puede estar vacio.");
		}else if(!validarContraseniaWeb()) {
			throw new BadPostCodeException("El campo de Contrasenia  no puede estar vacio.");
		}
		
	}

	public String getActivadaQueRealiza() {
		return activadaQueRealiza;
	}

	public void setActivadaQueRealiza(String activadaQueRealiza) {
		this.activadaQueRealiza = activadaQueRealiza;
	}

	public String getComprobanteIngresos() {
		return comprobanteIngresos;
	}

	public void setComprobanteIngresos(String comprobanteIngresos) {
		this.comprobanteIngresos = comprobanteIngresos;
	}

	public String getUsuarioWeb() {
		return usuarioWeb;
	}

	public void setUsuarioWeb(String usuarioWeb) {
		this.usuarioWeb = usuarioWeb;
	}

	public String getContraseniaWeb() {
		return contraseniaWeb;
	}

	public void setContraseniaWeb(String contraseniaWeb) {
		this.contraseniaWeb = contraseniaWeb;
	}
	
	
	public String toString(){
		return "Datos de Comprador: " + this.getApellido() + " " + this.getNombre() +", "+ this.getTipoDeDocumento() + ", "+ this.getDocumento()+ ", "+this.getEstadoCivil()+", " +this.getDomicilio() +
				", " + this.getEmail() +", " + this.getTelefono() +", "+ this.getUsuarioWeb() + ", " + this.getContraseniaWeb() + ", " + this.getComprobanteIngresos() +
				", " + this.getActivadaQueRealiza();
	
	}

	/*----------------metodos de validacion de comprador ------------------------*/
	
	public boolean validarActividadQueRealiza(){
 		boolean valido = false;
 		if(this.activadaQueRealiza.length()>6 && this.activadaQueRealiza.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarComprobanteIngreso(){
 		boolean valido = false;
 		if(this.comprobanteIngresos.length()>6 && this.comprobanteIngresos.length()<20){
 			valido = true;
 		}
 		return valido;
 	}
	
	public boolean validarUsuarioWeb(){
 		boolean valido = false;
 		if(this.usuarioWeb.length()>6 && this.usuarioWeb.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
	public boolean validarContraseniaWeb(){
 		boolean valido = false;
 		if(this.contraseniaWeb.length()>6 && this.contraseniaWeb.length()<9){
 			valido = true;
 		}
 		return valido;
 	}
}
