package modelo;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="Pago")

public class Pago {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_Pago", unique=true, updatable=false,nullable =false)
	private int cod_Pago;
	
	@Column(name="monto")
	private float monto;
	
	@Column(name="primerVencimiento")
    private LocalDate primerVencimiento;
	
	@Column(name="segundoVencimiento")
    private LocalDate segundoVencimiento;
	
	@Column(name="estadoDelPago")
    private boolean estadoDelPago;
	
	@Column(name="fechaDelPago")
    private LocalDate fechaDelPago;
	
	
	@ManyToOne
    private Contrato contrato;

    public Pago(float monto, LocalDate primerVencimiento, LocalDate segundoVencimiento, boolean estadoDelPago, LocalDate fechaDelPago, Contrato contrato) {
        this.monto = monto;
        this.primerVencimiento = primerVencimiento;
        this.segundoVencimiento = segundoVencimiento;
        this.estadoDelPago = estadoDelPago;
        this.fechaDelPago = fechaDelPago;
        this.contrato = contrato;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public void setPrimerVencimiento(LocalDate primerVencimiento) {
        this.primerVencimiento = primerVencimiento;
    }

    public void setSegundoVencimiento(LocalDate segundoVencimiento) {
        this.segundoVencimiento = segundoVencimiento;
    }

    public void setEstadoDelPago(boolean estadoDelPago) {
        this.estadoDelPago = estadoDelPago;
    }

    public void setFechaDelPago(LocalDate fechaDelPago) {
        this.fechaDelPago = fechaDelPago;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public float getMonto() {
        return monto;
    }

    public LocalDate getPrimerVencimiento() {
        return primerVencimiento;
    }

    public LocalDate getSegundoVencimiento() {
        return segundoVencimiento;
    }

    public boolean isEstadoDelPago() {
        return estadoDelPago;
    }

    public LocalDate getFechaDelPago() {
        return fechaDelPago;
    }

    public Contrato getContrato() {
        return contrato;
    }
}
