package modelo;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="fk_inmueble")
@Table(name="LocalesComerciales")
public class LocalComercial extends Inmueble implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToOne(mappedBy="localComercialAlquilado")
	private Alquiler alquiler;
	public LocalComercial(){}
	public LocalComercial(String ubicacion, int metrosCuadrados, boolean habilidato, Locador unLocador){
		super(ubicacion,metrosCuadrados,habilidato,unLocador);
	}
	
	
	public String toString(){
		return this.descripcionInmueble() + " Tipo: Local Comercial ";
	}
}
