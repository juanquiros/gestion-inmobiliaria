package main;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import controlador.Controlador;
import dao.Dao;
import excepcion.BadPostCodeException;
import modelo.Alquiler;
import modelo.ArancelEspecial;
import modelo.Casa;
import modelo.ComicionInmobiliaria;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;
import modelo.Recargo;
import vistas.viewPrincipal;

public class Main {

	public static void main(String[] args) {
		Dao dao = new Dao();
		Controlador controlador = new Controlador();
		
		Locador locador = null;
		Locatario locatario = null;
		Garante garante = null;
		
		try {
			garante = new Garante("DNI", "33222111", "Juan", "Quiros", "Soltero", "Corrientes",
					"3758-405436", "juanquiros@gmail.com", "Estudiante", "Comprobante pago");
			
			locador = new Locador("DNI", "44333222", "Tamara", "Prokopiw ", "Casada ", "libertad",
					"3758-405438", "tamara@gmail.com", "27443332229", "Tamara ");
			dao.Guardar(locador);
			locatario = new Locatario("DNI", "55444333", "Paula", "Rodriguez", "Soltera", "630",
					"3758-456590", "quiros@gmail.com", "Arquitecta", "Pago de haberes", "pauquiro", "Paupau01");
		} catch (BadPostCodeException e) {
			e.printStackTrace();
		}
		ComicionInmobiliaria comicionInmobiliaria = new ComicionInmobiliaria(3400, 12);
		Recargo recargo = new Recargo(450, 15, true);
		ArancelEspecial arancelEspecial = new ArancelEspecial("Luz", 345, true);
		Set<ArancelEspecial> arancelesEspeciales = new HashSet<ArancelEspecial>();
		arancelesEspeciales.add(arancelEspecial);
		Casa casa = new Casa("Hussay y Libertad", 60, true, locador);

		Alquiler alquiler = new Alquiler(LocalDate.now(), 12, true, comicionInmobiliaria, recargo, arancelesEspeciales,
				casa, null, null, garante, locatario);

		dao.Guardar(alquiler);
		
		viewPrincipal ventanaPrincipal = new viewPrincipal(controlador);
		ventanaPrincipal.setVisible(true);

	}

}
