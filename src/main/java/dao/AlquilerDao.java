package dao;

import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import modelo.Alquiler;
import util.HibernateUtil;

public class AlquilerDao {
	public Alquiler getAlquilerPorInmueble(String documento_) {   
		Alquiler alquiler = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Alquiler> cq = cb.createQuery(Alquiler.class);
        Root<Alquiler> rootEntry = cq.from(Alquiler.class);
        CriteriaQuery<Alquiler> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
        TypedQuery<Alquiler> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	alquiler = (Alquiler) allQuery.getResultList().get(0);
            }
        tx.commit();
        return alquiler ;       
    }
	public List<Alquiler> getAlquileres() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Alquileres", Alquiler.class).list();
		}
	}
}
