package dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelo.CuentaCorrienteCliente;
import util.HibernateUtil;

public class CuentaCorrienteClienteDao {
	public CuentaCorrienteCliente getCuentaPorDocumento(String documento_) {
	CuentaCorrienteCliente ccc = null; 
    Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
    Transaction tx = sesion.beginTransaction();
    CriteriaBuilder cb = sesion.getCriteriaBuilder();
    CriteriaQuery<CuentaCorrienteCliente> cq = cb.createQuery(CuentaCorrienteCliente.class);
    Root<CuentaCorrienteCliente> rootEntry = cq.from(CuentaCorrienteCliente.class);
    CriteriaQuery<CuentaCorrienteCliente> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
    TypedQuery<CuentaCorrienteCliente> allQuery = sesion.createQuery(all);        
    if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
    	ccc = (CuentaCorrienteCliente) allQuery.getResultList().get(0);
        }
    tx.commit();
    return ccc ;       
}
public List<CuentaCorrienteCliente> getClientes() {
	try (Session session = HibernateUtil.getSessionFactory().openSession()) {
		return session.createQuery("from CuentaCorrienteCliente", CuentaCorrienteCliente.class).list();
	}
}


}
