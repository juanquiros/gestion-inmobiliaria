package dao;

import java.io.Serializable;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class Dao {
	public void Guardar(Object dat) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			//transaction
			transaction = session.beginTransaction();			
			// 1
			Object object = session.save(dat);			
			// 2
			session.get(dat.getClass(), (Serializable) object);			
			// commit
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}	
	public void update(Object dat) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// transaction
			transaction = session.beginTransaction();
			// guardar un objeto
			session.update(dat);
			// commit
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}
	public void delete(Object dat) {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// transaction
			transaction = session.beginTransaction();
			// borrar un objeto
			session.delete(dat);
			// commit
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
	}
	
}
