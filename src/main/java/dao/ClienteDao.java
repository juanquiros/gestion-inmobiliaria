package dao;

import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import modelo.Comprador;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;
import util.HibernateUtil;


public class ClienteDao {
	public Locatario getLocatarioPorDocumento(String documento_) {   
		Locatario locatario = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Locatario> cq = cb.createQuery(Locatario.class);
        Root<Locatario> rootEntry = cq.from(Locatario.class);
        CriteriaQuery<Locatario> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
        TypedQuery<Locatario> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	locatario = (Locatario) allQuery.getResultList().get(0);
            }
        tx.commit();
        return locatario ;       
    }
	
	
	
	
	
	public Locador getLocadorPorDocumento(String documento_) {   
		Locador locador = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Locador> cq = cb.createQuery(Locador.class);
        Root<Locador> rootEntry = cq.from(Locador.class);
        CriteriaQuery<Locador> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
        TypedQuery<Locador> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	locador = (Locador) allQuery.getResultList().get(0);
            }
        tx.commit();
        return locador ;       
    }
	
	
	public Comprador getCompradorPorDocumento(String documento_) {   
		Comprador comprador = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Comprador> cq = cb.createQuery(Comprador.class);
        Root<Comprador> rootEntry = cq.from(Comprador.class);
        CriteriaQuery<Comprador> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
        TypedQuery<Comprador> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	comprador = (Comprador) allQuery.getResultList().get(0);
            }
        tx.commit();
        return comprador ;       
    }
	
	public Garante getGarantePorDocumento(String documento_) {   
		Garante garante = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Garante> cq = cb.createQuery(Garante.class);
        Root<Garante> rootEntry = cq.from(Garante.class);
        CriteriaQuery<Garante> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("documento"),documento_)); 
        TypedQuery<Garante> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
        	garante = (Garante) allQuery.getResultList().get(0);
            }
        tx.commit();
        return garante ;       
    }
	
	
	
	
	public List<Locatario> getLocatarios() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Locatario", Locatario.class).list();
		}
	}
	
	public List<Locador> getLocador() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Locador", Locador.class).list();
		}
	}
	public List<Comprador> getComprador() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Comprador", Comprador.class).list();
		}
	}
	public List<Garante> getGarante() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			return session.createQuery("from Garante", Garante.class).list();
		}
	}
}


