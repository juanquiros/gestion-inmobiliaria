package controlador;

import java.util.ArrayList;

import dao.AlquilerDao;
import dao.ClienteDao;
import dao.Dao;
import dao.InmuebleDao;
import excepcion.BadPostCodeException;
import modelo.Casa;
import modelo.Cliente;
import modelo.Comprador;
import modelo.Departamento;
import modelo.Garante;
import modelo.Inmueble;
import modelo.Locador;
import modelo.LocalComercial;
import modelo.Locatario;
import modelo.Terreno;

public class Controlador {
	InmuebleDao persistenciaInmuebles = new InmuebleDao();
	AlquilerDao persistenciaAlquiler = new AlquilerDao();
	ClienteDao persistenciaCliente = new ClienteDao ();
	Dao dao = new Dao ();

public ArrayList<Cliente> obtenerClientes(String documento_) {
	
	ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	
	Locatario unLocatario = persistenciaCliente.getLocatarioPorDocumento(documento_);
	Locador unLocador = persistenciaCliente.getLocadorPorDocumento(documento_);
	Comprador unComprador = persistenciaCliente.getCompradorPorDocumento(documento_);
	Garante unGarante = persistenciaCliente.getGarantePorDocumento(documento_);
	
	if(unLocatario!=null)clientes.add(unLocatario);
	if(unLocador!=null)clientes.add(unLocador);
	if(unComprador!=null)clientes.add(unComprador);
	if(unGarante!=null)clientes.add(unGarante);
	
	return clientes;
}
public Locador obtenerLocadorPorDocumento(String documento_) {
	Locador unLocatario = persistenciaCliente.getLocadorPorDocumento(documento_);
	return unLocatario;
}
public  ArrayList<Cliente> obtenerTodosLosClientes(){
	ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	
	ArrayList<Locatario> locatarios = (ArrayList<Locatario>) persistenciaCliente.getLocatarios();
	ArrayList<Locador> locadores = (ArrayList<Locador>) persistenciaCliente.getLocador();
	ArrayList<Comprador> compradores = (ArrayList<Comprador>) persistenciaCliente.getComprador();
	ArrayList<Garante> garantes = (ArrayList<Garante>) persistenciaCliente.getGarante();
	
	if(locatarios!=null)for(Locatario unLocatario: locatarios)clientes.add(unLocatario);
	if(locadores!=null)for(Locador unLocador: locadores)clientes.add(unLocador);
	if(compradores!=null)for(Comprador unComprador: compradores)clientes.add(unComprador);
	if(garantes!=null)for(Garante unGarante: garantes)clientes.add(unGarante);	
	
	return clientes;
}
/*------------------metodos dao de Cliente--------------------------------------*/
	public void  agregarCliente(Object cliente) throws BadPostCodeException {
			if(obtenerClientes(((Cliente) cliente).getDocumento()).size()>0){
				throw new BadPostCodeException("Ya se encuentra cargado este cliente");//BadPostCodeException
			}else{
				dao.Guardar(cliente);
			}		
	}
	public void actualizarCliente(Object Cliente){
		dao.update(Cliente);
	}
		public void borrarCliente(Object Cliente){
		dao.delete(Cliente);		
	}
/*------------------------------------------------------------------------------*/
	public ArrayList<Inmueble> obtenerTodosLosInmuebles() {
		ArrayList<Inmueble> Inmuebles = new ArrayList<Inmueble>();
		ArrayList<Casa> casas = (ArrayList<Casa>) persistenciaInmuebles.getCasas();
		ArrayList<Departamento> departamentos = (ArrayList<Departamento>) persistenciaInmuebles.getDepartamentos();
		ArrayList<LocalComercial> localesComerciales = (ArrayList<LocalComercial>) persistenciaInmuebles
				.getLocalesComerciales();
		ArrayList<Terreno> terrenos = (ArrayList<Terreno>) persistenciaInmuebles.getTerrenos();
		if (casas != null)
			for (Casa unaCasa : casas)
				Inmuebles.add(unaCasa);
		if (departamentos != null)
			for (Departamento unDepartamento : departamentos)
				Inmuebles.add(unDepartamento);
		if (localesComerciales != null)
			for (LocalComercial unLocalComercial : localesComerciales)
				Inmuebles.add(unLocalComercial);
		if (terrenos != null)
			for (Terreno unTerreno : terrenos)
				Inmuebles.add(unTerreno);
		return Inmuebles;
	}

	public ArrayList<Inmueble> obtenerInmueblePorM2yTipo(int min, int max, String tipoInmueble) {
		ArrayList<Inmueble> inmuebleEncontrados= new ArrayList<Inmueble>();

		ArrayList<Casa> casas= null;
		ArrayList<Departamento> departamentos = null;
		ArrayList<LocalComercial> localesComerciales = null;
		ArrayList<Terreno> terrenos = null;
		
		if (tipoInmueble.equals("Casa")) {
			casas = (ArrayList<Casa>) persistenciaInmuebles.getCasaPorM2(min, max);
			if (casas != null)
				for (Casa unaCasa : casas)
					inmuebleEncontrados.add(unaCasa);
		} else if (tipoInmueble.equals("Departamento")) {
			departamentos = (ArrayList<Departamento>) persistenciaInmuebles.getDepartamentoPorM2(min, max);
			if (departamentos != null)
				for (Departamento unDepartamento : departamentos)
					inmuebleEncontrados.add(unDepartamento);
		} else if (tipoInmueble.equals("Local Comercial")) {
			localesComerciales = (ArrayList<LocalComercial>) persistenciaInmuebles.getLocalComercialPorM2(min, max);
			if (localesComerciales != null)
				for (LocalComercial unLocalComercial : localesComerciales)
					inmuebleEncontrados.add(unLocalComercial);
		} else if (tipoInmueble.equals("Terreno")) {
			terrenos = (ArrayList<Terreno>) persistenciaInmuebles.getTerrenoPorM2(min, max);
			if (terrenos != null)
				for (Terreno unTerreno : terrenos)
					inmuebleEncontrados.add(unTerreno);
		} else {
			inmuebleEncontrados = null;
		}
		
		
		
		return inmuebleEncontrados;
	}

	public void agregarInmueble(Object inmueble) {
		dao.Guardar(inmueble);
	}

	public void actualizarInmueble(Object inmueble) {
		dao.update(inmueble);
	}

	public void borrarInmueble(Object inmueble) {
		dao.delete(inmueble);
	}
}		

