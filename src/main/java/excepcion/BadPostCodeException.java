package excepcion;

public class BadPostCodeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;//****
	public BadPostCodeException() {
        super();
    }

    public BadPostCodeException(String message) {
        super(message);
    }
}
