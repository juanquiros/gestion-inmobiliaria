package vistas;

import controlador.Controlador;
import excepcion.BadPostCodeException;
import modelo.Cliente;
import modelo.Comprador;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

/**
 *
 * @author Juan Quiros
 */
public class viewNuevoCliente extends javax.swing.JDialog {

	private static final long serialVersionUID = 1L;
	private javax.swing.JButton btnGuardar;
	private javax.swing.JButton btnCancelar;
	private javax.swing.JLabel lblNombre;
	private javax.swing.JLabel lblDomicilio;
	private javax.swing.JLabel lblEstadoCivil;
	private javax.swing.JLabel lblEmail;
	private javax.swing.JLabel lblContrasenia;
	private javax.swing.JLabel lblUsuario;
	private javax.swing.JLabel lblComprobante;
	private javax.swing.JLabel lblActividadRealiza;
	private javax.swing.JLabel lblDatosCliente;
	private javax.swing.JLabel lblApellido;
	private javax.swing.JLabel lblTelefono;
	private javax.swing.JLabel lblTipoDoc;
	private javax.swing.JLabel lblDocumento;
	private javax.swing.JLabel lblTipoCliente;
	private javax.swing.JLabel lblCuit;
	private javax.swing.JLabel lblRazon;
	private javax.swing.JLayeredPane jLayeredPane1;
	private javax.swing.JPanel PanelDatosPorTipo;
	private java.awt.Panel PanelDatosCliente;
	private javax.swing.JTextField txtActividad;
	private javax.swing.JTextField txtApellido;
	private javax.swing.JTextField txtComprobante;
	private javax.swing.JTextField txtContrasenia;
	private javax.swing.JTextField txtCuit;
	private javax.swing.JTextField txtDocumento;
	private javax.swing.JTextField txtDomicilio;
	private javax.swing.JTextField txtEmail;
	private javax.swing.JTextField txtEstadoCivil;
	private javax.swing.JTextField txtNombre;
	private javax.swing.JTextField txtRazonSocial;
	private javax.swing.JTextField txtTelefono;
	private javax.swing.JComboBox<String> comboTipoCliente;
	private javax.swing.JTextField txtTipoDoc;
	private javax.swing.JTextField txtUsuario;
	private Object cliente;
	private Controlador controlador;

	public viewNuevoCliente(java.awt.Frame parent, boolean modal, Controlador controlador, Cliente cliente) {
		super(parent, modal);
		setLocationRelativeTo(null);
		initComponents();
		this.controlador = controlador;
		if (cliente != null) {
			this.cliente = cliente;
			modoActualizarCliente();
		}
	}

	private void modoActualizarCliente() {
		this.btnGuardar.setText("Actualizar");
		txtDomicilio.setText(((Cliente) this.cliente).getDomicilio());
		txtEmail.setText(((Cliente) this.cliente).getEmail());
		txtEstadoCivil.setText(((Cliente) this.cliente).getEstadoCivil());
		txtTelefono.setText(((Cliente) this.cliente).getTelefono());
		txtTipoDoc.setText(((Cliente) this.cliente).getTipoDeDocumento());
		txtDocumento.setText(((Cliente) this.cliente).getDocumento());
		txtNombre.setText(((Cliente) this.cliente).getNombre());
		txtApellido.setText(((Cliente) this.cliente).getApellido());

		if (this.cliente instanceof Locador) {
			txtCuit.setText(((Locador) this.cliente).getCuit());
			txtRazonSocial.setText(((Locador) this.cliente).getRazonSocial());
			comboTipoCliente.setSelectedItem("Locador");
		}
		if (this.cliente instanceof Comprador) {
			txtUsuario.setText(((Comprador) this.cliente).getUsuarioWeb());
			txtActividad.setText(((Comprador) this.cliente).getActivadaQueRealiza());
			txtComprobante.setText(((Comprador) this.cliente).getComprobanteIngresos());
			txtContrasenia.setText(((Comprador) this.cliente).getContraseniaWeb());
			comboTipoCliente.setSelectedItem("Comprador");
		}
		if (this.cliente instanceof Locatario) {
			txtUsuario.setText(((Locatario) this.cliente).getUsuarioWeb());
			txtActividad.setText(((Locatario) this.cliente).getActividadQueRealiza());
			txtComprobante.setText(((Locatario) this.cliente).getComprobanteIngreso());
			txtContrasenia.setText(((Locatario) this.cliente).getContrasenia());
			comboTipoCliente.setSelectedItem("Locatario");
		}
		if (this.cliente instanceof Garante) {
			txtActividad.setText(((Garante) this.cliente).getActivadaQueRealiza());
			txtComprobante.setText(((Garante) this.cliente).getComprobanteIngresos());
			comboTipoCliente.setSelectedItem("Garante");
		}

		txtApellido.setEnabled(false);
		txtTipoDoc.setEnabled(false);
		txtCuit.setEnabled(false);
		txtDocumento.setEnabled(false);
		txtNombre.setEnabled(false);
		comboTipoCliente.setEnabled(false);
	}

	private void initComponents() {

		jLayeredPane1 = new javax.swing.JLayeredPane();
		PanelDatosCliente = new java.awt.Panel();
		lblNombre = new javax.swing.JLabel();
		lblApellido = new javax.swing.JLabel();
		lblTipoDoc = new javax.swing.JLabel();
		lblDocumento = new javax.swing.JLabel();
		lblTelefono = new javax.swing.JLabel();
		lblDomicilio = new javax.swing.JLabel();
		lblEstadoCivil = new javax.swing.JLabel();
		lblEmail = new javax.swing.JLabel();
		lblDatosCliente = new javax.swing.JLabel();
		txtNombre = new javax.swing.JTextField();
		txtApellido = new javax.swing.JTextField();
		txtTipoDoc = new javax.swing.JTextField();
		txtDocumento = new javax.swing.JTextField();
		txtEstadoCivil = new javax.swing.JTextField();
		txtTelefono = new javax.swing.JTextField();
		txtDomicilio = new javax.swing.JTextField();
		txtEmail = new javax.swing.JTextField();
		lblTipoCliente = new javax.swing.JLabel();
		comboTipoCliente = new javax.swing.JComboBox<>();
		PanelDatosPorTipo = new javax.swing.JPanel();
		btnCancelar = new javax.swing.JButton();
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				setVisible(false);
			}
		});
		txtDocumento.addKeyListener(new KeyAdapter() {
			   public void keyTyped(KeyEvent e)
			   {
			      char caracter = e.getKeyChar();
			      // Verificar si la tecla pulsada no es un digito
			      if(((caracter < '0') ||
			         (caracter > '9')) &&
			         (caracter != '\b' /*BACK_SPACE*/))
			      {
			         e.consume();  // ignorar el evento de teclado
			      }
			   }
			});
		btnGuardar = new javax.swing.JButton();
		btnGuardar.setText("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				botonGuardar();

			}
		});
		lblCuit = new javax.swing.JLabel();
		lblRazon = new javax.swing.JLabel();
		txtCuit = new javax.swing.JTextField();
		txtRazonSocial = new javax.swing.JTextField();
		txtActividad = new javax.swing.JTextField();
		txtComprobante = new javax.swing.JTextField();
		lblUsuario = new javax.swing.JLabel();
		lblComprobante = new javax.swing.JLabel();
		txtContrasenia = new javax.swing.JTextField();
		lblActividadRealiza = new javax.swing.JLabel();
		lblContrasenia = new javax.swing.JLabel();
		txtUsuario = new javax.swing.JTextField();

		PanelDatosCliente.setBackground(new java.awt.Color(240, 240, 240));

		lblNombre.setText("Nombre");
		lblDocumento.setText("Documento");
		lblTelefono.setText("Tel�fono");
		lblDomicilio.setText("Domicilio");
		lblApellido.setText("Apellido");
		lblEstadoCivil.setText("Estado Civil");
		lblTipoDoc.setText("Tipo doc");
		lblTipoDoc.setToolTipText("");
		lblEstadoCivil.setToolTipText("");
		lblEmail.setText("E-mail");
		lblDatosCliente.setText("Datos de Cliente");
		txtDomicilio.setToolTipText("");
		lblTipoCliente.setText("Tipo Cliente");

		lblDatosCliente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		
		comboTipoCliente.setModel(new javax.swing.DefaultComboBoxModel<>(
				new String[] { "Locador", "Locatario", "Garante", "Comprador" }));
		comboTipoCliente.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				txtTipoClienteItemStateChanged(evt);
			}
		});

		javax.swing.GroupLayout gl_PanelDatosCliente = new javax.swing.GroupLayout(PanelDatosCliente);
		PanelDatosCliente.setLayout(gl_PanelDatosCliente);
		gl_PanelDatosCliente.setHorizontalGroup(gl_PanelDatosCliente.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(gl_PanelDatosCliente.createSequentialGroup().addContainerGap().addGroup(gl_PanelDatosCliente
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(lblDatosCliente)
						.addGroup(gl_PanelDatosCliente.createSequentialGroup().addGroup(gl_PanelDatosCliente
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(gl_PanelDatosCliente.createSequentialGroup().addComponent(lblDocumento).addGap(18, 18, 18)
										.addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_PanelDatosCliente
										.createSequentialGroup()
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(lblNombre).addComponent(lblApellido)
												.addComponent(lblTipoDoc))
										.addGap(34, 34, 34)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(txtTipoDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
														javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_PanelDatosCliente.createSequentialGroup().addComponent(lblTipoCliente).addGap(18, 18, 18)
										.addComponent(comboTipoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 148,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addGap(76, 76, 76)
								.addGroup(gl_PanelDatosCliente.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(lblEstadoCivil).addComponent(lblDomicilio).addComponent(lblTelefono)
										.addComponent(lblEmail))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(gl_PanelDatosCliente.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(txtEstadoCivil, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(txtDomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
												javax.swing.GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(22, Short.MAX_VALUE)));
		gl_PanelDatosCliente.setVerticalGroup(gl_PanelDatosCliente.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, gl_PanelDatosCliente.createSequentialGroup()
						.addContainerGap().addComponent(lblDatosCliente)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(gl_PanelDatosCliente
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING,
										false)
								.addGroup(gl_PanelDatosCliente.createSequentialGroup()
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblNombre).addComponent(txtNombre,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblApellido).addComponent(txtApellido,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblTipoDoc).addComponent(txtTipoDoc,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblDocumento).addComponent(txtDocumento,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_PanelDatosCliente.createSequentialGroup()
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblEstadoCivil).addComponent(txtEstadoCivil,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblDomicilio).addComponent(txtDomicilio,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(lblTelefono).addComponent(txtTelefono,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(gl_PanelDatosCliente
												.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(lblEmail))))
						.addGap(18, 18, 18)
						.addGroup(gl_PanelDatosCliente.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblTipoCliente).addComponent(comboTipoCliente,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(22, Short.MAX_VALUE)));

		jLayeredPane1.setLayer(PanelDatosCliente, javax.swing.JLayeredPane.DEFAULT_LAYER);

		javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
		jLayeredPane1.setLayout(jLayeredPane1Layout);
		jLayeredPane1Layout.setHorizontalGroup(
				jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(PanelDatosCliente,
						javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		jLayeredPane1Layout.setVerticalGroup(
				jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane1Layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(PanelDatosCliente, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		btnCancelar.setText("Cancelar");
		lblCuit.setText("Cuit");
		lblRazon.setText("Razon Social");
		lblUsuario.setText("Usuario Web");
		lblComprobante.setText("Comprobante Ingreso");
		lblActividadRealiza.setText("Actividad que Realiza");
		lblContrasenia.setText("Contrase�a");
		
		txtComprobante.setToolTipText("");
		txtComprobante.setEnabled(false);
		txtActividad.setEnabled(false);
		lblUsuario.setEnabled(false);		
		lblComprobante.setEnabled(false);
		txtContrasenia.setEnabled(false);		
		lblActividadRealiza.setToolTipText("");
		lblActividadRealiza.setEnabled(false);
		lblContrasenia.setEnabled(false);
		txtUsuario.setEnabled(false);

		javax.swing.GroupLayout gl_PanelDatosPorTipo = new javax.swing.GroupLayout(PanelDatosPorTipo);
		PanelDatosPorTipo.setLayout(gl_PanelDatosPorTipo);
		gl_PanelDatosPorTipo.setHorizontalGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						gl_PanelDatosPorTipo.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 84,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 84,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap())
				.addGroup(gl_PanelDatosPorTipo.createSequentialGroup().addContainerGap()
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(lblCuit).addComponent(lblRazon))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
								javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(lblActividadRealiza).addComponent(lblComprobante).addComponent(lblUsuario)
								.addComponent(lblContrasenia))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(txtContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtActividad, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(txtComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 150,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(21, 21, 21)));
		gl_PanelDatosPorTipo.setVerticalGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, gl_PanelDatosPorTipo.createSequentialGroup()
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblActividadRealiza).addComponent(txtActividad,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblComprobante).addComponent(txtComprobante,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblUsuario).addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(lblContrasenia))
						.addGap(69, 69, 69)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 41,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 41,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap())
				.addGroup(gl_PanelDatosPorTipo.createSequentialGroup().addContainerGap()
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblCuit).addComponent(txtCuit, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(gl_PanelDatosPorTipo.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lblRazon).addComponent(txtRazonSocial,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jLayeredPane1).addComponent(PanelDatosPorTipo, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(PanelDatosPorTipo,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));

		pack();
	}// </editor-fold>

	
	private void txtTipoClienteItemStateChanged(java.awt.event.ItemEvent evt) {
		enableCompradorLocatario(false);
		this.txtCuit.setEnabled(false);
		this.txtRazonSocial.setEnabled(false);
		this.lblCuit.setEnabled(false);
		this.lblRazon.setEnabled(false);

		if (this.comboTipoCliente.getSelectedItem().toString().equals("Locador")) {
			this.lblCuit.setEnabled(true);
			this.txtCuit.setEnabled(true);
			this.lblRazon.setEnabled(true);
			this.txtRazonSocial.setEnabled(true);
		}
		if (this.comboTipoCliente.getSelectedItem().toString().equals("Locatario")
				|| this.comboTipoCliente.getSelectedItem().toString().equals("Comprador")) {
			enableCompradorLocatario(true);
		}
		if (this.comboTipoCliente.getSelectedItem().toString().equals("Garante")) {
			this.lblActividadRealiza.setEnabled(true);
			this.txtComprobante.setEnabled(true);
			this.lblComprobante.setEnabled(true);
			this.txtActividad.setEnabled(true);
		}
	}

	private void enableCompradorLocatario(boolean estado) {
		this.lblActividadRealiza.setEnabled(estado);
		this.txtComprobante.setEnabled(estado);
		this.lblComprobante.setEnabled(estado);
		this.txtActividad.setEnabled(estado);
		this.lblUsuario.setEnabled(estado);
		this.txtUsuario.setEnabled(estado);
		this.lblContrasenia.setEnabled(estado);
		this.txtContrasenia.setEnabled(estado);
	}

	private void botonGuardar() {
		if (this.cliente == null) {
			guardar();
		} else {
			actualizar();
		}

	}
	
	
	private void guardar(){
		try {
			if (this.comboTipoCliente.getSelectedItem().toString().equals("Locador")) {
				this.cliente = new Locador(this.txtTipoDoc.getText(), this.txtDocumento.getText(),
						this.txtNombre.getText(), this.txtApellido.getText(), this.txtEstadoCivil.getText(),
						this.txtDomicilio.getText(), this.txtTelefono.getText(), this.txtEmail.getText(),
						this.txtCuit.getText(), this.txtRazonSocial.getText());
			}
			if (this.comboTipoCliente.getSelectedItem().toString().equals("Locatario")) {
				this.cliente = new Locatario(this.txtTipoDoc.getText(), this.txtDocumento.getText(),
						this.txtNombre.getText(), this.txtApellido.getText(), this.txtEstadoCivil.getText(),
						this.txtDomicilio.getText(), this.txtTelefono.getText(), this.txtEmail.getText(),
						this.txtActividad.getText(), this.txtComprobante.getText(), this.txtUsuario.getText(),
						this.txtContrasenia.getText());
			}
			if (this.comboTipoCliente.getSelectedItem().toString().equals("Comprador")) {
				this.cliente = new Comprador(this.txtTipoDoc.getText(), this.txtDocumento.getText(),
						this.txtNombre.getText(), this.txtApellido.getText(), this.txtEstadoCivil.getText(),
						this.txtDomicilio.getText(), this.txtTelefono.getText(), this.txtEmail.getText(),
						this.txtActividad.getText(), this.txtComprobante.getText(), this.txtUsuario.getText(),
						this.txtContrasenia.getText());
			}
			if (this.comboTipoCliente.getSelectedItem().toString().equals("Garante")) {
				this.cliente = new Garante(this.txtTipoDoc.getText(), this.txtDocumento.getText(),
						this.txtNombre.getText(), this.txtApellido.getText(), this.txtEstadoCivil.getText(),
						this.txtDomicilio.getText(), this.txtTelefono.getText(), this.txtEmail.getText(),
						this.txtActividad.getText(), this.txtComprobante.getText());
			}
			this.controlador.agregarCliente(this.cliente);
			this.setVisible(false);
		} catch (BadPostCodeException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}
	private void actualizar() {
		((Cliente) this.cliente).setDomicilio(this.txtDomicilio.getText());
		((Cliente) this.cliente).setTelefono(this.txtTelefono.getText());
		((Cliente) this.cliente).setEmail(this.txtEmail.getText());
		((Cliente) this.cliente).setEstadoCivil(this.txtEstadoCivil.getText());
		if (this.comboTipoCliente.getSelectedItem().toString().equals("Locador")) {
			((Locador) this.cliente).setRazonSocial(this.txtRazonSocial.getText());
		}
		if (this.comboTipoCliente.getSelectedItem().toString().equals("Locatario")) {
			((Locatario) this.cliente).setActividadQueRealiza(this.txtActividad.getText());
			((Locatario) this.cliente).setComprobanteIngreso(this.txtComprobante.getText());
			((Locatario) this.cliente).setUsuarioWeb(this.txtUsuario.getText());
			((Locatario) this.cliente).setContrasenia(this.txtContrasenia.getText());
		}
		if (this.comboTipoCliente.getSelectedItem().toString().equals("Comprador")) {
			((Comprador) this.cliente).setActivadaQueRealiza(this.txtActividad.getText());
			((Comprador) this.cliente).setComprobanteIngresos(this.txtComprobante.getText());
			((Comprador) this.cliente).setUsuarioWeb(this.txtUsuario.getText());
			((Comprador) this.cliente).setContraseniaWeb(this.txtContrasenia.getText());
		}
		if (this.comboTipoCliente.getSelectedItem().toString().equals("Garante")) {
			((Garante) this.cliente).setActivadaQueRealiza(this.txtActividad.getText());
			((Garante) this.cliente).setComprobanteIngresos(this.txtComprobante.getText());
		}
		this.controlador.actualizarCliente(this.cliente);
		this.setVisible(false);
	}
}
