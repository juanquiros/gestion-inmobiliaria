package vistas;


import javax.swing.JFrame;

import controlador.Controlador;

import javax.swing.JToolBar;

import java.awt.BorderLayout;
import java.beans.PropertyVetoException;

import javax.swing.JDesktopPane;
import javax.swing.JButton;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

import javax.swing.JLabel;

public class viewPrincipal extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private viewClientes viewClientesPanel;
	private viewInmuebles viewInmuebles;
	
	
	private Controlador Controlador_1;
	//botones
	private JButton btnCompras;
	private JButton btnAlquileres;
	private JButton btnInmuebles;
	private JButton btnClientes;
	//etiquetas
	private JLabel lblProgramacinAvanzadaIi;
	 
	
	
	public viewPrincipal(Controlador c){
		
		//Configuracion de ventana
		super("Programación Avanzada II - Practico 3");
		setTitle("Programaci\u00F3n Avanzada II - Gesti\u00F3n Inmobiliaria");
		setResizable(false);
		setSize(800,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//Controlador
		this.Controlador_1 = c;
		
		
		//panel de cliente
		this.viewClientesPanel = new viewClientes(this,this.Controlador_1);
		this.viewClientesPanel.getContentPane().setBackground(SystemColor.activeCaption); //sin barra de titulo
		//panel de factura
		this.viewInmuebles = new viewInmuebles(this,this.Controlador_1);//fallta definir this.Controlador_1
		this.viewInmuebles.getContentPane().setBackground(SystemColor.activeCaption);
		
		
		
		//barra de menú
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		getContentPane().add(toolBar, BorderLayout.NORTH);//agregar al panel la barra
		toolBar.setBackground(SystemColor.inactiveCaption);
		toolBar.setBorder(null);
		
		//botones
		this.btnAlquileres = new JButton("Alquileres");
		this.btnCompras = new JButton("Ventas");
		this.btnClientes = new JButton("Clientes");
		this.btnInmuebles = new JButton("Inmuebles");
		btnAlquileres.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnCompras.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnClientes.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnInmuebles.setFont(new Font("Tahoma", Font.BOLD, 11));
		this.btnAlquileres.setBackground(SystemColor.inactiveCaptionBorder);
		this.btnCompras.setBackground(SystemColor.inactiveCaptionBorder);
		this.btnInmuebles.setBackground(SystemColor.inactiveCaptionBorder);
		this.btnClientes.setBackground(SystemColor.inactiveCaptionBorder);
		toolBar.add(this.btnInmuebles);
		toolBar.add(this.btnClientes);
		toolBar.add(this.btnAlquileres);
		toolBar.add(this.btnCompras);		
		
		
		
		//acciones de los botones
		this.btnCompras.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				
			}
		});		
		this.btnAlquileres.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				
			}
		});	
		this.btnClientes.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mostrarClientes();
			}
			
		});		
		this.btnInmuebles.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mostrarInmuebles();
			}
		});
		
		
		//panel para las ventanas 
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(SystemColor.activeCaption);
		getContentPane().add(desktopPane, BorderLayout.CENTER);
		
		//agregar los view al panel
		desktopPane.add(this.viewInmuebles);
		desktopPane.add(this.viewClientesPanel);
		
		lblProgramacinAvanzadaIi = new JLabel("Programaci\u00F3n Avanzada II - Practico Integrador");
		lblProgramacinAvanzadaIi.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 30));
		lblProgramacinAvanzadaIi.setBounds(24, 389, 779, 61);
		desktopPane.add(lblProgramacinAvanzadaIi);
		
		
		
	}

	private void mostrarClientes(){
		if(!this.viewClientesPanel.isVisible()){
			cerrarTodoLosView();
			this.setTitle(this.viewClientesPanel.getTitle());
			this.viewClientesPanel.setVisible(true);
			this.btnClientes.setBackground(SystemColor.activeCaption);
			try {
				this.viewClientesPanel.setMaximum(true);
			        } catch(PropertyVetoException e) { e.printStackTrace(); } 
		}else{
			this.setTitle("Programación Avanzada II - Practico 3");
			this.viewClientesPanel.setVisible(false);
			this.btnClientes.setBackground(SystemColor.inactiveCaptionBorder);
		}
		
	}
	private void mostrarInmuebles(){
		if(!this.viewInmuebles.isVisible()){
			cerrarTodoLosView();
			this.setTitle(this.viewInmuebles.getTitle());
			this.viewInmuebles.setVisible(true);
			this.btnInmuebles.setBackground(SystemColor.activeCaption);
			try {
				this.viewInmuebles.setMaximum(true);
			        } catch(PropertyVetoException e) { e.printStackTrace(); } 
		}else{
			this.setTitle("Programación Avanzada II - Practico 3");
			this.viewInmuebles.setVisible(false);
			this.btnInmuebles.setBackground(SystemColor.inactiveCaptionBorder);
		}
		
	}
	
	private void cerrarTodoLosView(){
		this.viewInmuebles.setVisible(false);
		this.viewClientesPanel.setVisible(false);
		this.btnCompras.setBackground(SystemColor.inactiveCaptionBorder);
		this.btnAlquileres.setBackground(SystemColor.inactiveCaptionBorder);
		this.btnInmuebles.setBackground(SystemColor.inactiveCaptionBorder);
		this.btnClientes.setBackground(SystemColor.inactiveCaptionBorder);
	}
}
