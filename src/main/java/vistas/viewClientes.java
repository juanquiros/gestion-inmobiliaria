/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import controlador.Controlador;
import modelo.Cliente;
import modelo.Comprador;
import modelo.Garante;
import modelo.Locador;
import modelo.Locatario;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;

/**
 *
 * @author Juan Quiros
 */
public class viewClientes extends  JInternalFrame  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Variables declaration - do not modify
	private javax.swing.JButton jButton1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel21;
	private javax.swing.JLabel jLabel22;
	private javax.swing.JLayeredPane jLayeredPane1;
	private javax.swing.JMenu jMenu1;
	private javax.swing.JMenu jMenu2;
	private javax.swing.JMenu jMenu3;
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable jTable1;
	private javax.swing.JTextField jTextField1;
	//ventana principal
		private Frame parent;
	private ArrayList<Cliente> datosDeTabla = new ArrayList<Cliente>();
	private Controlador controlador;
	private JMenu mnVerDetalles;
	
	public viewClientes(Frame parent,Controlador controlador) {
		setTitle("Clientes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 664, 459);
		((javax.swing.plaf.basic.BasicInternalFrameUI)this.getUI()).setNorthPane(null);
		initComponents();
		this.controlador = controlador;
		this.parent= parent;
		datosDeTabla = this.controlador.obtenerTodosLosClientes();
		cargarTabla(datosDeTabla);
	}

	private void initComponents() {

		jLayeredPane1 = new javax.swing.JLayeredPane();
		jPanel2 = new javax.swing.JPanel();
		jLabel22 = new javax.swing.JLabel();
		jLabel1 = new javax.swing.JLabel();
		jTextField1 = new javax.swing.JTextField();
		jButton1 = new javax.swing.JButton();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTable1 = new javax.swing.JTable();
		jLabel21 = new javax.swing.JLabel();
		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu1 = new javax.swing.JMenu();
		jMenu2 = new javax.swing.JMenu();
		jMenu3 = new javax.swing.JMenu();
		jButton1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				jButton1MousePressed();
			}
		});
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jLabel22.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel22.setText("Buscar Cliente");

		jLabel1.setText("Documento");

		jButton1.setText("Buscar");

		jTable1.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null } },
				new String[] { "Tipo Cliente", "Nombre", "Apellido", "Tipo documento", "Documento" }));
		jScrollPane1.setViewportView(jTable1);

		jLabel21.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
		jLabel21.setText("Datos de Clientes");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup().addComponent(jLabel21).addGap(0, 0,
										Short.MAX_VALUE))
								.addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
										javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE))
						.addContainerGap()));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(jLabel21)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jScrollPane1,
								javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(26, 26, 26)));

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jLabel22)
								.addGroup(jPanel2Layout.createSequentialGroup().addComponent(jLabel1)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 124,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(jButton1)))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(jLabel22)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1)
								.addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(jButton1))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 236,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jLayeredPane1.setLayer(jPanel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

		javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
		jLayeredPane1.setLayout(jLayeredPane1Layout);
		jLayeredPane1Layout.setHorizontalGroup(
				jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jPanel2,
						javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		jLayeredPane1Layout
				.setVerticalGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jLayeredPane1Layout.createSequentialGroup()
								.addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(0, 0, Short.MAX_VALUE)));

		jMenu1.setText("Nuevo");
		jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				jMenu1MousePressed();
			}
		});
		jMenuBar1.add(jMenu1);

		jMenu2.setText("Editar");
		jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				jMenu2MenuKeyPressed();
			}
		});
		jMenuBar1.add(jMenu2);

		jMenu3.setText("Borrar");
		jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				jMenu3MenuKeyPressed();
			}
		});
		jMenuBar1.add(jMenu3);

		setJMenuBar(jMenuBar1);

		mnVerDetalles = new JMenu("Ver Detalles");
		mnVerDetalles.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				mnVerDetallesMenuKeyPressed();
			}
		});
		jMenuBar1.add(mnVerDetalles);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLayeredPane1));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		pack();
	}// </editor-fold>

	private void jMenu1MousePressed() {
		viewNuevoCliente viewNuevo;
		viewNuevo = new viewNuevoCliente(this.parent, true, this.controlador, null);
		viewNuevo.setVisible(true);
		datosDeTabla = this.controlador.obtenerTodosLosClientes();
		cargarTabla(datosDeTabla);
	}

	private void jMenu2MenuKeyPressed() {
		int indice = jTable1.getSelectedRow();
		viewNuevoCliente viewNuevo;
		if (indice > -1) {
			viewNuevo = new viewNuevoCliente(this.parent, true, this.controlador, this.datosDeTabla.get(indice));
			viewNuevo.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente para editar");
		}
		datosDeTabla = this.controlador.obtenerTodosLosClientes();
		cargarTabla(datosDeTabla);
	}

	private void jMenu3MenuKeyPressed() {
		int indice = jTable1.getSelectedRow();
		if (indice > -1 && JOptionPane.showConfirmDialog(null, "Seguro quiere eliminar al Cliente seleccionado?",
				"Borrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			this.controlador.borrarCliente(this.datosDeTabla.get(indice));
			jButton1MousePressed();// Actualizar Tabla
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente para borrar");
		}
		datosDeTabla = this.controlador.obtenerTodosLosClientes();
		cargarTabla(datosDeTabla);
	}

	private void jButton1MousePressed() {
		if(jTextField1.getText().length()>0){
			this.datosDeTabla = controlador.obtenerClientes(jTextField1.getText());
		}else{
			this.datosDeTabla = controlador.obtenerTodosLosClientes();
		}
		
		cargarTabla(this.datosDeTabla);
	}

	private void mnVerDetallesMenuKeyPressed() {
		int indice = jTable1.getSelectedRow();
		if (indice > -1) {
			JOptionPane.showMessageDialog(null, this.datosDeTabla.get(indice).toString());
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente para ver todos los detalles");
		}
	}

	private void cargarTabla(ArrayList<Cliente> Clientes) {
		Vector<Object> fila;
		DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
		for (int i = dtm.getRowCount(); i > 0; i--) {
			dtm.removeRow(i - 1);
		}
		if (Clientes != null) {
			for (Cliente cliente : Clientes) {
				fila = new Vector<Object>();
				if (cliente instanceof Locador) {
					fila.add("Locador");
				}
				if (cliente instanceof Comprador) {
					fila.add("Comprador");
				}
				if (cliente instanceof Locatario) {
					fila.add("Locatario");
				}
				if (cliente instanceof Garante) {
					fila.add("Garante");
				}
				fila.add(cliente.getNombre());
				fila.add(cliente.getApellido());
				fila.add(cliente.getTipoDeDocumento());
				fila.add(cliente.getDocumento());
				dtm.addRow(fila);
			}
		}
	}

}
