package util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.SessionFactory;

public class HibernateUtil {
	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;
	
	public static SessionFactory getSessionFactory(){
		if(sessionFactory == null){
			try{
				//Crear registro
				registry = new StandardServiceRegistryBuilder().configure().build();
				//Metadatos 
				MetadataSources sources = new MetadataSources(registry);
				Metadata metadata = sources.getMetadataBuilder().build();
				//crear session
				sessionFactory = metadata.getSessionFactoryBuilder().build();
			} catch (Exception e){
				e.printStackTrace();
				if(registry!=null){
					StandardServiceRegistryBuilder.destroy(registry);
				}
			}
		}
		return sessionFactory;		
	}
	public static void shutdown(){
		if(registry!=null){
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
